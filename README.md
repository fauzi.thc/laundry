 Final Project

 Kelompok 8

 Muhammad Fauzi Ramdhani

 Abdullah Ahmad Fauzi 

 Fikri Irsyad Mufhida 

 Tema Project -> Laundry

 ERD

![erd_laundry](/uploads/e418cd75f0efdc1702546b5223990980/erd_laundry.png)

 Link

 Demo Aplikasi -> https://youtu.be/QjbCHJMzqII

 Deploy Aplikasi -> http://jcc-laundry.herokuapp.com/

 Untuk login saya sertakan akun di bawah ini dengan 2 role. karena ada role admin dan pegawai. dimana admin bisa crud user dan role user sedangkan pegawai tidak bisa.

 Role admin 

  email -> akucintakonoha@gmail.com

  email -> fauzi.thc@gmail.com

 Role pegawai
 
  email -> fikri@gmail.com

  Semua akun di atas memiliki password yang sama yaitu 12345678


